/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');

const problem1 = dirName => {
  fs.mkdir(`./${dirName}`, err => {
    if (err) {
      console.error(err);
    } else {
      console.log(`mkdir: created directory ${dirName}`);
      const file1 = 'file1.json';

      fs.writeFile(`./${dirName}/${file1}`, '', err => {
        if (err) {
          console.error(err);
        } else {
          console.log(`created file ${file1}`);

          const file2 = 'file2.json';

          fs.writeFile(`./${dirName}/${file2}`, '', err => {
            if (err) {
              console.error(err);
            } else {
              console.log(`created file ${file2}`);

              fs.unlink(`${dirName}/${file1}`, err => {
                if (err) {
                  console.error(err);
                } else {
                  console.log(`${file1} deleted`);
                }
              });

              fs.unlink(`${dirName}/${file2}`, err => {
                if (err) {
                  console.error(err);
                } else {
                  console.log(`${file2} deleted`);
                }
              });
            }
          });
        }
      });
    }
  });
};
module.exports = problem1;
