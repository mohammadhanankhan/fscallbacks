const fs = require('fs');

const problem2 = () => {
  // 1
  fs.readFile('./data/lipsum.txt', 'utf8', (err, data) => {
    if (err) {
      console.error(err);
    } else {
      // 2
      const upperCaseFile = 'upperCaseFile';

      fs.writeFile(`./${upperCaseFile}`, data.toUpperCase(), err => {
        if (err) {
          console.error(err);
        } else {
          // console.log('Written to upperCaseFile.');

          const fileNames = 'filesNames.txt';

          fs.writeFile(`./${fileNames}`, `${upperCaseFile}\n`, err => {
            if (err) {
              console.error(err);
            } else {
              console.log(`${upperCaseFile} stored in ${fileNames}`);
              // 3
              fs.readFile(`./${upperCaseFile}`, 'utf8', (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  const lowerCaseSentences = data
                    .toLowerCase()
                    .split('. ')
                    .join('\n');
                  const lowerCaseFile = 'lowerCaseFile';

                  fs.writeFile(
                    `./${lowerCaseFile}`,
                    lowerCaseSentences,
                    err => {
                      if (err) {
                        console.error(err);
                      } else {
                        // console.log('Written to lowerCaseFile');
                        fs.appendFile(
                          `./${fileNames}`,
                          `${lowerCaseFile}\n`,
                          err => {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log(
                                `${lowerCaseFile} stored in ${fileNames}`
                              );

                              fs.readFile(
                                `./${lowerCaseFile}`,
                                'utf8',
                                (err, lowerCaseData) => {
                                  if (err) {
                                    console.error(err);
                                  } else {
                                    fs.readFile(
                                      `./${upperCaseFile}`,
                                      'utf8',
                                      (err, upperCaseData) => {
                                        if (err) {
                                          console.error(err);
                                        } else {
                                          const sortedUpperCaseData =
                                            upperCaseData
                                              .split('')
                                              .sort()
                                              .join('')
                                              .trim();

                                          const sortedLowerCaseData =
                                            lowerCaseData
                                              .split('')
                                              .sort()
                                              .join('')
                                              .trim();

                                          const sortedFile = 'sortedFile';
                                          fs.writeFile(
                                            `./${sortedFile}`,
                                            `${sortedLowerCaseData}\n${sortedUpperCaseData}`,
                                            err => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                fs.appendFile(
                                                  `./${fileNames}`,
                                                  `${sortedFile}`,
                                                  err => {
                                                    if (err) {
                                                      console.error(err);
                                                    } else {
                                                      fs.readFile(
                                                        `./${fileNames}`,
                                                        'utf8',
                                                        (err, data) => {
                                                          if (err) {
                                                            console.error(err);
                                                          } else {
                                                            const fileNames =
                                                              data.split('\n');

                                                            fileNames.forEach(
                                                              file => {
                                                                fs.unlink(
                                                                  `./${file}`,
                                                                  err => {
                                                                    if (err) {
                                                                      console.error(
                                                                        err
                                                                      );
                                                                    } else {
                                                                      console.log(
                                                                        `removed file ${file}`
                                                                      );
                                                                    }
                                                                  }
                                                                );
                                                              }
                                                            );
                                                          }
                                                        }
                                                      );
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    }
  });
};
module.exports = problem2;
